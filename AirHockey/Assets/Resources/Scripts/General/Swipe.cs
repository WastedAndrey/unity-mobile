﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SwipePhase
{ 
    Nothing, 
    Began,
    Moved,
    Ended
}

public class Swipe : MonoBehaviour
{
    static SwipePhase swipePhase = SwipePhase.Nothing;
    static List<Vector2> points = new List<Vector2>();
    
    public static SwipePhase SwipePhase { get { return swipePhase; } }
    public static List<Vector2> Points { get { return new List<Vector2>(points); } }


    // Start is called before the first frame update
    void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    points.Clear();
                    swipePhase = SwipePhase.Began;
                    points.Add(Camera.main.ScreenToWorldPoint(touch.position));
                    break;
                case TouchPhase.Moved:
                    swipePhase = SwipePhase.Moved;
                    points.Add(Camera.main.ScreenToWorldPoint(touch.position));
                    break;
                case TouchPhase.Ended:
                    points.Add(Camera.main.ScreenToWorldPoint(touch.position));
                    swipePhase = SwipePhase.Ended;
                    break;
            }
        }
        else
        {
            swipePhase = SwipePhase.Nothing;
        }
    }
}
