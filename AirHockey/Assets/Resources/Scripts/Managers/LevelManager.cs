﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LevelStage
{ 
    PlayerInput,
    StrikeAnimation,
    WaitForNewGame
}

public delegate void GameEvent();

public class LevelManager : MonoBehaviour
{
    private static LevelStage levelStage;
    public static LevelStage LevelStage { get { return levelStage; } }
    

    [SerializeField]
    List<GameObject> gameLevelsPrefabs = new List<GameObject>();
    [SerializeField]
    GameObject messageUI;

    int levelNumber;
    Level activeLevel;


    public LevelStage _levelStage;

    // Start is called before the first frame update
    void Start()
    {
        InitLevel(0);
    }

    // Update is called once per frame
    void Update()
    {
        _levelStage = LevelStage;
    }

    void InitLevel(int levelIndex)
    {
        levelIndex = Mathf.Clamp(levelIndex, 0, gameLevelsPrefabs.Count-1);

        levelStage = LevelStage.PlayerInput;

        levelNumber = levelIndex;

        if (activeLevel != null) Destroy(activeLevel.gameObject);

        GameObject newLevel = Instantiate(gameLevelsPrefabs[levelIndex], Vector3.zero, Quaternion.identity);
        activeLevel = newLevel.GetComponent<Level>();
        

        activeLevel.win += Win;
        activeLevel.loose += Loose;

        activeLevel.Init(this);
        messageUI.GetComponent<MessageUI>().RecieveMessage($"Уровень: {levelIndex+1}");
    }

    void Win()
    {
        if (levelStage != LevelStage.WaitForNewGame)
        {
            levelStage = LevelStage.WaitForNewGame;

            if (levelNumber + 1 < gameLevelsPrefabs.Count)
                messageUI.GetComponent<MessageUI>().RecieveMessage("Уровень пройден!");
            else
                messageUI.GetComponent<MessageUI>().RecieveMessage("Вы прошли игру!");

            GameAnalytics.NewDesignEvent("level_complete", levelNumber);

            StartCoroutine(WinCoroutine());
        }
    }

    void Loose()
    {
        if (levelStage != LevelStage.WaitForNewGame)
        {
            levelStage = LevelStage.WaitForNewGame;
            messageUI.GetComponent<MessageUI>().RecieveMessage("Вы проиграли :(");
            StartCoroutine(LooseCoroutine());

            GameAnalytics.NewDesignEvent("level_fail", levelNumber);
        }
        
    }

    IEnumerator WinCoroutine()
    {
        yield return new WaitForSeconds(5);

        if (levelNumber + 1 < gameLevelsPrefabs.Count)
            InitLevel(levelNumber + 1);
        else
            InitLevel(0);


    }

    IEnumerator LooseCoroutine()
    {
        yield return new WaitForSeconds(5);

        InitLevel(0);
    }

    public void SetStage(LevelStage stage)
    {
        levelStage = stage;
    }
}
