﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManger : MonoBehaviour
{
    [SerializeField]
    GameObject prefabPlayer;
    [SerializeField]
    GameObject prefabEnemy;
    [SerializeField]
    GameObject prefabWinZone;

    [SerializeField]
    GameObject prefabExplosion;


    public GameObject PrefabPlayer { get { return prefabPlayer; } }
    public GameObject PrefabEnemy { get { return prefabEnemy; } }
    public GameObject PrefabWinZone { get { return prefabWinZone; } }

    public GameObject PrefabExplosion { get { return prefabExplosion; } }

    private static PrefabManger _instance;

    public static PrefabManger Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PrefabManger>();
            }

            return _instance;
        }
    }
}
