﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    GameObject faceObj;
    [SerializeField]
    GameObject modelObj;
    [SerializeField]
    List<Sprite> faceSprites = new List<Sprite>();

    Rigidbody2D rigidBody;

    // Start is called before the first frame update
    void Awake()
    {
        int rnd = Random.Range(0, faceSprites.Count);
        faceObj.GetComponent<SpriteRenderer>().sprite = faceSprites[rnd];

        rigidBody = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePhysics();
    }
    void UpdatePhysics()
    {
        Vector2 velocity = rigidBody.velocity;


        if (Mathf.Abs(velocity.x) > 0.1)
            velocity.x += 0.5f * Time.deltaTime * -Mathf.Sign(velocity.x);
        else
            velocity.x = 0;

        if (Mathf.Abs(velocity.y) > 0.1)
            velocity.y += 0.5f * Time.deltaTime * -Mathf.Sign(velocity.y);
        else
            velocity.y = 0;


        if (Mathf.Abs(velocity.x) <= 0.1) velocity.x = 0;
        if (Mathf.Abs(velocity.y) <= 0.1) velocity.y = 0;

        rigidBody.velocity = velocity;
    }
}
