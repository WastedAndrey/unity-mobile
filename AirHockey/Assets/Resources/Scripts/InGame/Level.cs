﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [SerializeField]
    List<GameObject> enemySpawnZones = new List<GameObject>();

    [SerializeField]
    List<GameObject> winAreaSpawnZones = new List<GameObject>();

    [SerializeField]
    int winAreasCount;

    [SerializeField]
    int enemiesCount;


    private LevelManager levelManager;
    private GameObject player;
    private List<GameObject> enemies = new List<GameObject>();
    private List<GameObject> winAreas = new List<GameObject>();

    public event GameEvent win;
    public event GameEvent loose;

    float freezeLevelStageTimer = 0;

    public void Init(LevelManager levelManager)
    {
        this.levelManager = levelManager;

        foreach (Transform child in this.transform)
        {
            if (child.tag == "Player")
            {
                player = child.gameObject;
                player.GetComponent<Player>().win += Win;
                player.GetComponent<Player>().loose += Loose;
                player.GetComponent<Player>().strike += Strike;
            }

            if (child.tag == "Enemy")
            {
                enemies.Add(child.gameObject);
            }

            if (child.tag == "EnemySpawnZone")
            {
                enemySpawnZones.Add(child.gameObject);
            }

            if (child.tag == "WinAreaSpawnZone")
            {
                winAreaSpawnZones.Add(child.gameObject);
            }
        }

        if (winAreasCount > 0) CreateWinArea();

        if (enemiesCount > 0) CreateRandomEnemy();
    }

    // Update is called once per frame
    void Update()
    {
        if (freezeLevelStageTimer > 0) freezeLevelStageTimer -= Time.deltaTime;
    }

    private void FixedUpdate()
    {
        if (LevelManager.LevelStage == LevelStage.StrikeAnimation)
            UpdateStageStrikeAnimation();
    }

    void CreateRandomEnemy()
    {
        if (enemySpawnZones.Count == 0)
        {
            Debug.Log("No zones for enemy spawn.");
            return;
        }

        for (int i = 0; i < enemiesCount; i++)
        {
            int zoneIndex = Random.Range(0, enemySpawnZones.Count);

            Vector3 pos = Vector3.zero;
            pos.x = Random.Range(enemySpawnZones[zoneIndex].transform.position.x - enemySpawnZones[zoneIndex].transform.localScale.x / 2, enemySpawnZones[zoneIndex].transform.position.x + enemySpawnZones[zoneIndex].transform.localScale.x / 2);
            pos.y = Random.Range(enemySpawnZones[zoneIndex].transform.position.y - enemySpawnZones[zoneIndex].transform.localScale.y / 2, enemySpawnZones[zoneIndex].transform.position.y + enemySpawnZones[zoneIndex].transform.localScale.y / 2);

            GameObject newEnemy = Instantiate(GameManager.Instance.Prefabs.PrefabEnemy, pos, Quaternion.identity);
            newEnemy.transform.SetParent(this.transform);
            enemies.Add(newEnemy);
        }
    }

    void CreateWinArea()
    {
        if (winAreaSpawnZones.Count == 0)
        {
            Debug.Log("No zones for winArea spawn.");
            return;
        }

        for (int i = 0; i < winAreasCount; i++)
        {
            int zoneIndex = Random.Range(0, winAreaSpawnZones.Count);

            Vector3 pos = Vector3.zero;
            pos.x = Random.Range(winAreaSpawnZones[zoneIndex].transform.position.x - winAreaSpawnZones[zoneIndex].transform.localScale.x / 2, winAreaSpawnZones[zoneIndex].transform.position.x + winAreaSpawnZones[zoneIndex].transform.localScale.x / 2);
            pos.y = Random.Range(winAreaSpawnZones[zoneIndex].transform.position.y - winAreaSpawnZones[zoneIndex].transform.localScale.y / 2, winAreaSpawnZones[zoneIndex].transform.position.y + winAreaSpawnZones[zoneIndex].transform.localScale.y / 2);

            GameObject newWinArea = Instantiate(GameManager.Instance.Prefabs.PrefabWinZone, pos, Quaternion.identity);
            newWinArea.transform.SetParent(this.transform);
            winAreas.Add(newWinArea);
        }
    }

    void Win()
    {
        win?.Invoke();
    }

    void Loose()
    {
        loose?.Invoke();
    }

    void Strike()
    {
        if (LevelManager.LevelStage == LevelStage.PlayerInput)
        {
            levelManager.SetStage(LevelStage.StrikeAnimation);
            freezeLevelStageTimer = 2;
        }      
    }

    void UpdateStageStrikeAnimation()
    {
        if (Vector2.Distance(Vector2.zero, player.GetComponent<Rigidbody2D>().velocity) < 0.1f 
            && freezeLevelStageTimer <= 0)
                levelManager.SetStage(LevelStage.PlayerInput);
    }
}
