﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Player : MonoBehaviour
{
    public event GameEvent win;
    public event GameEvent loose;
    public event GameEvent strike;

    [SerializeField]
    GameObject strikeArrow;
    [SerializeField]
    GameObject model;
    [SerializeField]
    GameObject shadow;

    Rigidbody2D rigidBody;
    LevelManager levelManager;

    public float forceMultiplier = 10;
    float noBrakesTimer = 0;
    // Start is called before the first frame update
    void Start()
    {
        rigidBody = this.GetComponent<Rigidbody2D>();
        this.GetComponent<Animator>().speed = 0.15f;
    }

    public void Init(LevelManager levelManager)
    {
        this.levelManager = levelManager;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateInput();

        model.transform.rotation = Quaternion.Euler(0,0,0);
        shadow.transform.rotation = Quaternion.Euler(0,0,0);
    }


    private void FixedUpdate()
    {
        UpdatePhysics();
    }

    /// <summary>
    /// Апдейт ввода игрока. Применяет силу и передает необходимые параметры стрелке.
    /// </summary>
    void UpdateInput()
    {
        if (LevelManager.LevelStage == LevelStage.PlayerInput)
        {
            if (Swipe.SwipePhase == SwipePhase.Began)
            {
                strikeArrow.GetComponent<StrikeArrow>().Enable();
            }

            if (Swipe.SwipePhase == SwipePhase.Moved)
            {
                if (strikeArrow.activeSelf == false) strikeArrow.GetComponent<StrikeArrow>().Enable();

                Vector2 dir = (Swipe.Points[Swipe.Points.Count - 1] - Swipe.Points[0]).normalized;
                float dist = Vector2.Distance(Swipe.Points[Swipe.Points.Count - 1], Swipe.Points[0]);
                dist = Mathf.Clamp(dist, 1.5f, 6f);
                strikeArrow.GetComponent<StrikeArrow>().SetParams(dir, dist / 2);
            }

            if (Swipe.SwipePhase == SwipePhase.Ended)
            {
                strikeArrow.GetComponent<StrikeArrow>().Disable();
                Vector2 dir = (Swipe.Points[Swipe.Points.Count - 1] - Swipe.Points[0]).normalized;
                float dist = Vector2.Distance(Swipe.Points[Swipe.Points.Count - 1], Swipe.Points[0]);
                dist = Mathf.Clamp(dist, 1.5f, 6f);

                rigidBody.AddForce(dir * dist * forceMultiplier);
                strike?.Invoke();
            }
        }
        else
           if (strikeArrow.activeSelf) strikeArrow.GetComponent<StrikeArrow>().Disable();
    }

    /// <summary>
    /// Апдейт физики игрока. В данном случае просто постепенно замедляет и останавливает игрока
    /// </summary>
    void UpdatePhysics()
    {
        if (LevelManager.LevelStage == LevelStage.StrikeAnimation ||
            LevelManager.LevelStage == LevelStage.PlayerInput)
        {
            Vector2 velocity = rigidBody.velocity;

            velocity.x = velocity.x - velocity.x * Time.fixedDeltaTime / 2;
            velocity.y = velocity.y - velocity.y * Time.fixedDeltaTime / 2;


            float velositySpeed = Vector2.Distance(Vector2.zero, velocity);
            if (velositySpeed < 0.1f) velocity = Vector2.zero;

            rigidBody.velocity = velocity;

            rigidBody.angularVelocity -= rigidBody.angularVelocity * Time.fixedDeltaTime * 0.25f;
        }

        if (LevelManager.LevelStage == LevelStage.WaitForNewGame)
            rigidBody.velocity = Vector3.zero;
       
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (LevelManager.LevelStage == LevelStage.WaitForNewGame) return;

        if (collision.gameObject.tag == "Enemy")
        {
            loose?.Invoke();
            Instantiate(GameManager.Instance.Prefabs.PrefabExplosion, collision.GetContact(0).point, Quaternion.identity);
            AudioManager.Instance.PlayLoose();
        }
        if (collision.gameObject.tag == "Border")
        { 
            Instantiate(GameManager.Instance.Prefabs.PrefabExplosion, collision.GetContact(0).point, Quaternion.identity);
            AudioManager.Instance.PlayExplosion();
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (LevelManager.LevelStage == LevelStage.WaitForNewGame) return;

        if (col.gameObject.tag == "WinArea")
        {
            win?.Invoke();
            Instantiate(GameManager.Instance.Prefabs.PrefabExplosion, this.transform.position, Quaternion.identity);
            AudioManager.Instance.PlayWin();
        }
    }
}
