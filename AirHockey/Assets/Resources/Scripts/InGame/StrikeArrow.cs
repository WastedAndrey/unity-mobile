﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrikeArrow : MonoBehaviour
{
    [SerializeField]
    GameObject strikeArrowSpriteObj;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetParams(Vector2 direction, float dist)
    {
        float rotZ= Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        rotZ -= this.transform.parent.rotation.eulerAngles.z;
        this.transform.localRotation = Quaternion.Euler(0f, 0f, rotZ - 90);

        Vector3 localScale = strikeArrowSpriteObj.transform.localScale;
        localScale.y = dist;
        strikeArrowSpriteObj.transform.localScale = localScale;
    }

    public void Enable()
    {
        this.gameObject.SetActive(true);
    }

    public void Disable()
    {
        Vector3 localScale = strikeArrowSpriteObj.transform.localScale;
        localScale.y = 0;
        strikeArrowSpriteObj.transform.localScale = localScale;
        this.gameObject.SetActive(false);
    }
}


