﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageUI : MonoBehaviour
{

    public float defaultMessageTime = 3;

    float hideTimer = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (hideTimer > 0)
        {
            hideTimer -= Time.deltaTime;
            if (hideTimer <= 0) this.GetComponent<Animator>().SetBool("Shown", false);
        }
        

    }
    public void RecieveMessage(string text)
    {
        RecieveMessage(text, defaultMessageTime);
    }
    public void RecieveMessage(string text, float time)
    {
        this.GetComponent<Animator>().SetBool("Shown", true);
        this.GetComponent<Text>().text = text;
        hideTimer = time;
    }
}
