﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTap : MonoBehaviour
{
    public GameObject target;
    List<Color> colors;

    // Start is called before the first frame update
    void Start()
    {
        colors = new List<Color> { Color.red, Color.blue, Color.white };
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
                target.GetComponent<SpriteRenderer>().color = colors[Random.Range(0, 3)];
        }

        if (Input.GetMouseButtonDown(0))
        {
            target.GetComponent<SpriteRenderer>().color = colors[Random.Range(0, 3)];

        }
    }
}
