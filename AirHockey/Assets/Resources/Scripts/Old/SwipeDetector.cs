﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    public static List<SwipeData> Swipes { get; }

    private List<SwipeData> swipes = new List<SwipeData>();
    private bool alreadyDetecting;
    private int touchIndex = 0;


    [SerializeField]
    private float minSwipeDist = 20;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTouches();
    }


    void UpdateTouches()
    {

        for (int i = 0; i < Input.touches.Length; i++)
        {
            var touch = Input.touches[i];

            UpdateTouch(touch, i);
        }

        // Очищает ненужные касания
        for (int i = 0; i < swipes.Count; i++)
        {
            if (swipes[i] == null)
            {
                swipes.RemoveAt(i);
                if (i < touchIndex) touchIndex--;
            }
        }
    }

    void UpdateTouch(Touch touch, int index)
    {
        if (alreadyDetecting == true && touchIndex == index)
        {
            UpdateActiveTouch(touch, index);
            return;
        }

        if (alreadyDetecting == true && touchIndex != index)
        {
            UpdateNotActiveTouch(touch, index);
            return;
        }

        if (alreadyDetecting == false)
        {
            UpdateTouchNooneDetected(touch, index);
            return;
        }
    }

    void UpdateTouchNooneDetected(Touch touch, int index)
    {
        if (touch.phase == TouchPhase.Began)
        {
            if (swipes.Count <= index) swipes.Add(new SwipeData(touch.position));
        }
        else if (touch.phase == TouchPhase.Moved)
        {
            swipes[index].AddPoint(touch.position);

            if (swipes[index].GetStraightDistance() >= minSwipeDist)
            {
                alreadyDetecting = true;
                touchIndex = index;
            }
        }
        else if (touch.phase == TouchPhase.Ended)
        {
            swipes[index].AddPoint(touch.position);
            if (swipes[index].GetStraightDistance() >= minSwipeDist)
            {
                alreadyDetecting = true;
                touchIndex = index;
                DetectSwipe();
            }
            else
            {
                swipes[index] = null;
            }
        }
    }

    void UpdateNotActiveTouch(Touch touch, int index)
    {
        if (touch.phase == TouchPhase.Ended)
        {
            swipes[index] = null;
        }
    }

    void UpdateActiveTouch(Touch touch, int index)
    {
        if (touch.phase == TouchPhase.Moved)
        {
            swipes[index].AddPoint(touch.position);
        }
        else if (touch.phase == TouchPhase.Ended)
        {
            swipes[index].AddPoint(touch.position);
            DetectSwipe();
        }
    }

    void DetectSwipe()
    { 
    
    }
}
