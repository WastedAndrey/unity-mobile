﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SwipeDirection
{ 
    Left,
    Right,
    Up,
    Down
}

public class SwipeData
{
    List<Vector2> points;
    bool finished = false;
    public List<Vector2> Points { get { return new List<Vector2>(points); } }
    public SwipeData(Vector2 start)
    {
        points = new List<Vector2>();
        points.Add(start);
    }

    public void AddPoint(Vector2 point)
    {
        if (!finished)
            points.Add(point);
    }

    public void FinishSwipe()
    {
        finished = true;
    }

    public float GetStraightDistance()
    {
        return Vector2.Distance(points[0], points[points.Count - 1]);
    }

    public float GetOverallDistance()
    {
        float distance = 0;
        for (int i = 0; i < points.Count - 1; i++)
        {
            distance += Vector2.Distance(points[i], points[i + 1]);
        }
        return distance;
    }

    public SwipeDirection GetDirection()
    {
        Vector2 dir = points[points.Count - 1] - points[0];

        if (Mathf.Abs(dir.x) >= Mathf.Abs(dir.y))
        {
            if (dir.x >= 0)
                return SwipeDirection.Right;
            else return SwipeDirection.Left;
        }
        else
        {
            if (dir.y >= 0)
                return SwipeDirection.Up;
            else return SwipeDirection.Down;
        }
    }
}
