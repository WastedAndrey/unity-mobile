﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum SwipeLineMode
{ 
    OnlyLast,
    All
}
public class SwipeLine : MonoBehaviour
{
    public SwipeLineMode swipeLineMode;

    LineRenderer lineRenderer;

    // Start is called before the first frame update
    void Awake()
    {
        lineRenderer = this.gameObject.GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (swipeLineMode) 
        {
            case SwipeLineMode.OnlyLast:
                {
                    int count = Swipe.Points.Count;
                    if (count >= 2)
                    {
                        Vector3[] points = new Vector3[2];
                        points[0] = new Vector3(Swipe.Points[0].x, Swipe.Points[0].y, -1);
                        points[1] = new Vector3(Swipe.Points[count - 1].x, Swipe.Points[count - 1].y, -1);
                        lineRenderer.positionCount = 2;
                        lineRenderer.SetPositions(points);
                        break;
                    }
                    else
                    {
                        lineRenderer.positionCount = count;
                        break;
                    }
                }
                

            case SwipeLineMode.All:
                {
                    Vector3[] points = new Vector3[Swipe.Points.Count];

                    if (Swipe.Points.Count > 0)
                    {
                        for (int i = 0; i < Swipe.Points.Count; i++)
                        {
                            points[i] = new Vector3(Swipe.Points[i].x, Swipe.Points[i].y, -1);
                        }
                    }

                    lineRenderer.positionCount = Swipe.Points.Count;
                    lineRenderer.SetPositions(points);
                    break;
                }
            default: break;
        }
    }

    
}
