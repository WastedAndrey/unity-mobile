﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ProgressBar : MonoBehaviour
{
    [SerializeField]
    GameObject prefabNode;
    [SerializeField]
    GameObject prefabText;
    // Start is called before the first frame update
    List<GameObject> nodes = new List<GameObject>(); // малые ноды для каждого отдельного уровня
    List<GameObject> coreNodes = new List<GameObject>();  // большие ноды, которые обрамляют номера уровней
    List<GameObject> levelTexts = new List<GameObject>();  // номера уровней в виде текста
    Vector2 sizeDelta = Vector2.zero;
    int activeLevelNumber = 0;

    public Color completed = new Color(0.8f, 0.60f, 0.35f, 1);
    public Color active = new Color(0.6f, 0.75f, 0.35f, 1);
    public Color notCompleted = new Color(0.35f, 0.35f, 0.35f, 1);

    void Awake()
    {
        for (int i = 0; i < 2; i++)
        {
            GameObject newNode = Instantiate(prefabNode, Vector3.zero, Quaternion.identity);
            newNode.transform.SetParent(this.gameObject.transform, false);
            coreNodes.Add(newNode);
        }

        for (int i = 0; i < 2; i++)
        {
            GameObject newText = Instantiate(prefabText, Vector3.zero, Quaternion.identity);
            newText.transform.SetParent(this.gameObject.transform, false);
            levelTexts.Add(newText);
        }

    }

    private void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<RectTransform>().sizeDelta != sizeDelta)
        {
            UpdateNodesTransform();
            UpdateCoreNodesTransform();
        }
    }

    public void Init(int count)
    {
        for (int i = 0; i < nodes.Count; i++)
        {
            Destroy(nodes[i]);
        }
        nodes.Clear();

        for (int i = 0; i < count; i++)
        {
            GameObject newNode = Instantiate(prefabNode, Vector3.zero, Quaternion.identity);
            newNode.transform.SetParent(this.gameObject.transform, false);
            nodes.Add(newNode);
        }

        UpdateNodesTransform();
        UpdateCoreNodesTransform();
    }

    public void LoadLevel(int number, bool showNext)
    {
        activeLevelNumber = number;
        UpdateNodesColors();

        levelTexts[0].GetComponent<TextMeshProUGUI>().text = (number+1).ToString();
        levelTexts[1].GetComponent<TextMeshProUGUI>().text = (number + 2).ToString();

        if (showNext == true) levelTexts[1].SetActive(true);
        else levelTexts[1].SetActive(false);
    }

    void UpdateNodesTransform()
    {
        Vector2 size = this.GetComponent<RectTransform>().sizeDelta;
        int count = nodes.Count;
        for (int i = 0; i < count; i++)
        {
            Vector2 widthHeight = Vector2.zero;
            widthHeight.x = size.x / (count + 4);
            widthHeight.y = size.y * 0.50f;

            Vector2 pos = Vector2.zero;
            pos.x = widthHeight.x * (i + 2);

            nodes[i].GetComponent<RectTransform>().anchoredPosition = pos;
            nodes[i].GetComponent<RectTransform>().sizeDelta = widthHeight;
        }
    }

    void UpdateCoreNodesTransform()
    {
        Vector2 size = this.GetComponent<RectTransform>().sizeDelta;
        int count = nodes.Count;

        {
            Vector2 widthHeight = Vector2.zero;
            widthHeight.x = size.x / (count + 4);
            widthHeight.y = size.y * 0.75f;

            Vector2 pos = Vector2.zero;
            pos.x = widthHeight.x * 0.5f;
            widthHeight.x *= 1.5f;

            coreNodes[0].GetComponent<RectTransform>().anchoredPosition = pos;
            coreNodes[0].GetComponent<RectTransform>().sizeDelta = widthHeight;

            levelTexts[0].GetComponent<RectTransform>().anchoredPosition = pos;
            levelTexts[0].GetComponent<RectTransform>().sizeDelta = widthHeight;
            levelTexts[0].GetComponent<TextMeshProUGUI>().fontSize = Mathf.Min(widthHeight.x, widthHeight.y) * 0.75f;
        }

        {
            Vector2 widthHeight = Vector2.zero;
            widthHeight.x = size.x / (count + 4);
            widthHeight.y = size.y * 0.75f;

            Vector2 pos = Vector2.zero;
            pos.x = widthHeight.x * (count + 2f);
            widthHeight.x *= 1.5f;

            coreNodes[1].GetComponent<RectTransform>().anchoredPosition = pos;
            coreNodes[1].GetComponent<RectTransform>().sizeDelta = widthHeight;

            levelTexts[1].GetComponent<RectTransform>().anchoredPosition = pos;
            levelTexts[1].GetComponent<RectTransform>().sizeDelta = widthHeight;
            levelTexts[1].GetComponent<TextMeshProUGUI>().fontSize = Mathf.Min(widthHeight.x, widthHeight.y) * 0.75f;
        }
    }

    void UpdateNodesColors()
    {
        coreNodes[0].GetComponent<Image>().color = completed;
        coreNodes[1].GetComponent<Image>().color = notCompleted;

        for (int i = 0; i < nodes.Count; i++)
        {
            if (i < activeLevelNumber)
                nodes[i].GetComponent<Image>().color = completed;

            if (i == activeLevelNumber)
                nodes[i].GetComponent<Image>().color = active;

            if (i > activeLevelNumber)
                nodes[i].GetComponent<Image>().color = notCompleted;
        }
    }
}
