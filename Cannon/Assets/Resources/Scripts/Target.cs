﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public float lowestPosY;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckPos();
    }

    void CheckPos()
    {
        if (this.transform.position.y < lowestPosY)
            Destroy(this.gameObject);
    }
}
