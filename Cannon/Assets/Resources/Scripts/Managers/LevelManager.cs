﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField]
    List<GameObject> levels = new List<GameObject>();
    // Start is called before the first frame update

    [SerializeField]
    GameObject player;

    [SerializeField]
    GameObject messageUI;

    [SerializeField]
    GameObject levelProgressBarUI;

    int levelIndex = 0;
    GameObject currentLevel;

    void Start()
    {
        player.GetComponent<Player>().loose += Loose;
        levelProgressBarUI.GetComponent<ProgressBar>().Init(levels.Count);
        LoadLevel(levelIndex);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) Win();
    }

    void LoadLevel(int index)
    {
        if (currentLevel != null) Destroy(currentLevel);
        index = Mathf.Clamp(index, 0, levels.Count - 1);

        player.GetComponent<Player>().Init(index * 3 + 3);

        GameObject newLevel = Instantiate(levels[index], Vector3.zero, Quaternion.identity);
        newLevel.GetComponent<Level>().win += Win;
        currentLevel = newLevel;

        messageUI.GetComponent<MessageUI>().RecieveMessage($"Уровень: {index+1}");
        bool show = index < levels.Count - 1 ? true : false;
        levelProgressBarUI.GetComponent<ProgressBar>().LoadLevel(index, show);
    }

    void Win()
    {
        AudioManager.Instance.PlayWin();
        FacebookManager.Instance.LogWinEvent(levelIndex);
        messageUI.GetComponent<MessageUI>().RecieveMessage("Вы победили !!!");
        StartCoroutine(WinCoroutine());
    }

    IEnumerator WinCoroutine()
    {
        yield return new WaitForSeconds(4);
        levelIndex++;
        if (levelIndex > levels.Count - 1) levelIndex = 0;
        LoadLevel(levelIndex);
    }

    void Loose()
    {
        AudioManager.Instance.PlayLoose();
        FacebookManager.Instance.LogLooseEvent(levelIndex);
        messageUI.GetComponent<MessageUI>().RecieveMessage("Вы проиграли :(");
        StartCoroutine(LooseCoroutine());
    }

    IEnumerator LooseCoroutine()
    {
        yield return new WaitForSeconds(4);

        if (levelIndex > levels.Count - 1) levelIndex = 0;
        LoadLevel(levelIndex);
    }
}
