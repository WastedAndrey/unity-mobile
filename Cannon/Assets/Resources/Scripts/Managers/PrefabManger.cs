﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManger : MonoBehaviour
{
    [SerializeField]
    GameObject prefabCannonball;
    [SerializeField]
    GameObject prefabExplosion;



    public GameObject PrefabCannonball { get { return prefabCannonball; } }
    public GameObject PrefabExplosion { get { return prefabExplosion; } }


    private static PrefabManger _instance;

    public static PrefabManger Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PrefabManger>();
            }

            return _instance;
        }
    }
}
