﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    private PrefabManger prefabManger;
    private AudioManager audioManager;
    public PrefabManger Prefabs { get { return prefabManger; } }
    public AudioManager Audio { get { return audioManager; } }
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (this.GetComponent<PrefabManger>() == null) this.gameObject.AddComponent<PrefabManger>();
        prefabManger = this.GetComponent<PrefabManger>();

        if (this.GetComponent<AudioManager>() == null) this.gameObject.AddComponent<AudioManager>();
        audioManager = this.GetComponent<AudioManager>();

        if (this.GetComponent<AudioSource>() == null) this.gameObject.AddComponent<AudioSource>();
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }


}

