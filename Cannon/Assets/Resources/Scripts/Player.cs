﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    GameObject ballSpawnPoint;
    [SerializeField]
    GameObject barrel;
    [SerializeField]
    GameObject barrelAngleRing;
    [SerializeField]
    GameObject attachedCamera;
    [SerializeField]
    GameObject ballsCount;



    public float forceMultiplier = 550;
    public float reloadTime = 1.5f;
    public GameEvent loose;

    float reloadTimeLeft;
    int ballsLeft;


    [SerializeField]
    float barrelAngle;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Init(int ballsNumber)
    {
        ballsLeft = ballsNumber;
        ballsCount.GetComponent<TextMeshPro>().text = ballsLeft.ToString();
        this.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateInput();
        UpdateBarrel();

        if (reloadTimeLeft > 0) reloadTimeLeft -= Time.deltaTime;
    }

    void UpdateInput()
    {
        if (Input.GetMouseButtonDown(0))
        { // if left button pressed...
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.tag == "Target")
                {
                    Shoot(hit.point);
                }
            }
        }


        if (Swipe.SwipePhase == SwipePhase.Moved && Swipe.IsVertical)
        {
            float dist = Swipe.Points[Swipe.Points.Count - 1].y - Swipe.Points[Swipe.Points.Count - 2].y;
            barrelAngle -= dist;
        }

        if (Swipe.SwipePhase == SwipePhase.Moved && Swipe.IsHorizontal)
        {
            float dist = Swipe.Points[Swipe.Points.Count - 1].x - Swipe.Points[Swipe.Points.Count - 2].x;
            Vector3 cameraAngle = Vector3.zero;
            cameraAngle.y = attachedCamera.transform.rotation.eulerAngles.y - dist;

            if (cameraAngle.y > 50 && cameraAngle.y < 358) cameraAngle.y = 358;
            if (cameraAngle.y > 2 && cameraAngle.y < 50) cameraAngle.y = 2;
            attachedCamera.transform.rotation = Quaternion.Euler(cameraAngle);
        }

        if (Input.GetKey(KeyCode.W))
            barrelAngle+= 10 * Time.deltaTime;

        if (Input.GetKey(KeyCode.S))
            barrelAngle -= 10 * Time.deltaTime;

        
    }

    void UpdateBarrel()
    {
        barrelAngle = Mathf.Clamp(barrelAngle, -5, 15);

        var barrelRotation = barrel.transform.rotation.eulerAngles;
        barrelRotation.x = barrelAngle;
        barrel.transform.rotation = Quaternion.Euler(barrelRotation);

        var barrelRingRotation = new Vector3(barrelAngle * 15, 0, 90);
        barrelAngleRing.transform.rotation = Quaternion.Euler(new Vector3(barrelRingRotation.x, barrelRingRotation.y, barrelRingRotation.z));
    }

    void Shoot(Vector3 pos)
    {
        if (reloadTimeLeft <= 0)
        {
            if (ballsLeft == 0)
            {
                loose?.Invoke();
                this.enabled = false;
                return;
            }

            GameObject newBall = Instantiate(PrefabManger.Instance.PrefabCannonball, ballSpawnPoint.transform.position, Quaternion.identity);
            Vector3 forceDir = pos - ballSpawnPoint.transform.position;
            forceDir = forceDir.normalized;
            forceDir.y += barrelAngle / 40+0.15f;
            forceDir = forceDir.normalized;
            newBall.GetComponent<Rigidbody>().AddForce(forceDir.normalized * forceMultiplier);
            ballsLeft--;
            ballsCount.GetComponent<TextMeshPro>().text = ballsLeft.ToString();

            Instantiate(PrefabManger.Instance.PrefabExplosion, ballSpawnPoint.transform.position, Quaternion.identity);

            reloadTimeLeft = reloadTime;

            AudioManager.Instance.PlayExplosion();
        }
    }
}
