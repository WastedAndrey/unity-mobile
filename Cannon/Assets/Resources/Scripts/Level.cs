﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void GameEvent();
public class Level : MonoBehaviour
{
    public List<GameObject> targets = new List<GameObject>();
    public GameEvent win;
    // Start is called before the first frame update
    void Start()
    {
        foreach (Transform childTR in this.transform)
        {
            if (childTR.gameObject.tag == "Target")
                targets.Add(childTR.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if (targets[i] == null) targets.RemoveAt(i);
        }

        if (targets.Count <= 0) 
        {
            win?.Invoke();
            this.enabled = false;
        }
        
    }
}
